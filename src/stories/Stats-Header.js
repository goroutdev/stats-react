import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import StatsHeader from './../components/Stats-Header';
import { plays } from './data';


storiesOf('Stats Header', module)
  .addDecorator(withKnobs)
  .add('Header Display for Stats', () => <StatsHeader plays={plays} callTypeFilter={select('Call Type', [1, 2, 3, 4], 1)} />);
