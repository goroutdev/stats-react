import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import MetricDisplay from './../components/Metric-Display';


storiesOf('MetricDisplay', module)
  .addDecorator(withKnobs)
  .add('Display for stats metrics', () => <MetricDisplay data={text('Data', '28')} label={text('Label', 'Plays Ran')} />);
