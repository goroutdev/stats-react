import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import { plays } from './data';
import RatingDisplay from './../components/Rating-Display';

const options = {
  type: [
    'positive',
    'negative',
  ],
  callTypeFilter: [1, 2, 3, 4],
};
storiesOf('Rating Block', module)
  .addDecorator(withKnobs)
  .add('Display for Rating Percentage', () => <RatingDisplay type={select('Type', options.type, 'positive')} plays={plays} callTypeFilter={select('Call Type', options.callTypeFilter, 1)} />);
