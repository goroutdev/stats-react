import React from 'react';
import { buttons } from './data';
import { storiesOf, action } from '@kadira/storybook';
import ButtonGroup from './../components/Button-Group';

storiesOf('ButtonGroup', module)
  .add('Drop Down', () => (
    <ButtonGroup data={buttons} />
  ));
