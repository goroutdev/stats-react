import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import PlayCardGroup from './../components/Play-Card-Group';
import { plays } from './data';


storiesOf('Play Card Group', module)
  .addDecorator(withKnobs)
  .add('Group Display of Play Cards', () => <PlayCardGroup practiceIndex="-KkN2jbSWSEsQH3XoCsE" sendPlay={action('sendPlay')} changeRating={action('changeRating')} href="https://air.gorout.net" plays={plays} callTypeFilter={select('Call Type', [1, 2, 3, 4], 1)} ratingFilter={select('Rating', [0, 1, 2, 3], 3)} />);
