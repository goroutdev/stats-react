import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import PlayCard from './../components/Play-Card';

storiesOf('Play Card', module)
  .addDecorator(withKnobs)
  .add('with no rating', () => <PlayCard href="https://air.gorout.net" changeRating={action('Change Rating')} sendPlay={action('Send Play')} organizationId={2} image="playfile0_5903c7df05b20-6_send.jpg" time={12915} name="Spread Right Flex 2 H Trap Lead" rating={select('Rating', { 3: 'None', 1: 'Positive', 0: 'Negative' }, '3')} />);
