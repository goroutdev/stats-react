import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import { practices } from './data';
import StatsList from './../components/Stats-List';

storiesOf('Stats List', module)
  .addDecorator(withKnobs)
  .add('Display List of Practices', () => <StatsList practices={practices} open={action('open')} delete={action('delete-actoin')} />);
