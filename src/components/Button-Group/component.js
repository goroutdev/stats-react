import React from 'react';
import PropTypes from 'prop-types';

function ButtonContainer(props) {
  const buttonClasses = props.active ? 'btn-group active open' : 'btn-group';

  return (
    <div id="buttongroupcomponent" className={buttonClasses}>
      {props.children}
    </div>
  );
}
ButtonContainer.displayName = 'ButtonContainer';

function ButtonHeader(props) {
  return (
    <div>
      <button type="button" className="btn type-title-text dropdown-toggle">
        {props.data[props.activeIndex].text}
      </button>
      <button type="button" className="btn type-title-arrow" onClick={props.openList}>
        <i className="gr-icon icon-down-dir" />
      </button>
    </div>
  );
}
ButtonHeader.displayName = 'ButtonHeader';

function ListItem(props) {
  const classes = props.active ? 'gr-icon icon-circle' : 'gr-icon icon-circle-thin';
  const classClickTest = props.clickTest ? 'listItemClickTest' : '';

  return (
    <li className={classClickTest} onClick={() => { props.setActive(props.index); }}>
      <i className={classes} /> {props.text}
    </li>
  );
}
ListItem.displayName = 'ListItem';

function List(props) {
  let i = 0;
  const listItems = props.data.map((row) => {
    const listItem = <ListItem key={i} index={i} active={(props.activeIndex === i)} clickTest={i === 3} text={row.text} setActive={props.setActive} />;
    i++;
    return listItem;
  });
  return (
    <ul className="type-title dropdown-menu animation-expand" role="menu">
      {listItems}
    </ul>
  );
}
List.displayName = 'List';

export default class ButtonGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      activeIndex: 0,
    };
    this.openList = this.openList.bind(this);
    this.setActive = this.setActive.bind(this);
  }

  setActive(i) {
    this.setState({ activeIndex: i, active: false });
    if (typeof this.props.setActive === 'function') {
      this.props.setActive(i);
    }
  }

  openList() {
    if (this.state.active) {
      this.setState({ active: false });
    } else {
      this.setState({ active: true });
    }
  }
  render() {
    return (
      <ButtonContainer active={this.state.active}>
        <ButtonHeader data={this.props.data} activeIndex={this.state.activeIndex} openList={this.openList} />
        <List data={this.props.data} activeIndex={this.state.activeIndex} setActive={this.setActive} />
      </ButtonContainer>
    );
  }
}

ButtonGroup.propTypes = {
  data: PropTypes.array.isRequired,
};
