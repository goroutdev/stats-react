import React from 'react';
import PropTypes from 'prop-types';

export const COMPONENT = class SOME_COMPONENT extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <template />
    );
  }
};

COMPONENT.propTypes = {
};

export default COMPONENT;
