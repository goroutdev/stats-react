import React from 'react';
import PropTypes from 'prop-types';
import PlayCard from './../Play-Card/component';

export const COMPONENT = class PlayCardGroup extends React.Component {
  constructor(props) {
    super(props);
    this.getFilteredPlays = this.getFilteredPlays.bind(this);
    this.filterByCallType = this.filterByCallType.bind(this);
  }

  filterByCallType() {
    const plays = this.props.plays;
    const callType = this.props.callTypeFilter;
    const _plays = {};
    Object.keys(plays).forEach((key) => {
      const play = plays[key];
      if (play.callType == callType) {
        _plays[key] = play;
      }
    });

    return _plays;
  }

  filterByRating(plays) {
    const rating = this.props.ratingFilter;
    const _plays = {};
    Object.keys(plays).forEach((key) => {
      const play = plays[key];
      if (rating == play.rating) {
        _plays[key] = play;
      } else if (rating == 2 && !play.isRated) {
        _plays[key] = play;
      }
    });
    return _plays;
  }

  getFilteredPlays() {
    let plays = this.filterByCallType();
    if (this.props.ratingFilter < 3) {
      plays = this.filterByRating(plays);
    }
    return plays;
  }

  render() {
    const href = this.props.href ? this.props.href : window.location.origin;
    const plays = this.getFilteredPlays();
    const playCards = Object.keys(plays).map(key => <PlayCard sendPlay={() => this.props.sendPlay(plays[key], { playIndex: key, practiceIndex: this.props.practiceIndex })} changeRating={() => this.props.changeRating(plays[key], { playIndex: key, practiceIndex: this.props.practiceIndex })} key={key} href={href} organizationId={plays[key].organizationId} image={plays[key].image} time={plays[key].interval} rating={plays[key].rating} name={plays[key].name} />);

    return (
      <div className="playcardgroup">
        {playCards}
      </div>
    );
  }
};

COMPONENT.propTypes = {
};

export default COMPONENT;
