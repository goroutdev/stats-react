import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

export const COMPONENT = class RatingDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.drawChart = this.drawChart.bind(this);
    this.drawCharts = this.drawCharts.bind(this);
    this.filterByCallType = this.filterByCallType.bind(this);
  }

  componentDidMount() {
    this.drawCharts();
  }

  componentDidUpdate(prevProps, prevState) {
    const selector = `#${this.props.type === 'positive' ? 'positive' : 'negative'} .piechart`;
    d3.selectAll(selector).remove();
    this.drawCharts();
  }

  filterByCallType() {
    const plays = this.props.plays;
    const callType = this.props.callTypeFilter;
    const _plays = {};
    Object.keys(plays).forEach((key) => {
      const play = plays[key];
      if (play.callType == callType) {
        _plays[key] = play;
      }
    });

    return _plays;
  }

  drawCharts() {
    const plays = this.filterByCallType();
    const el = this.props.type === 'positive' ? '#positive' : '#negative';
    const rating = this.props.type === 'positive' ? 1 : 0;
    let count = 0;
    Object.keys(plays).forEach(function (key) {
      const play = plays[key];
      if (play.isRated && play.rating == rating) {
        count += 1;
      }
    });
    this.drawChart(el, ((count / Object.keys(plays).length) * 100));
  }

  drawChart(element, percent) {
    if (isNaN(percent)) { percent = 0; }
    let width = 250;
    let height = 250;
    let text_y = '.35em';
    width = typeof width !== 'undefined' ? width : 250;
    height = typeof height !== 'undefined' ? height : 250;
    text_y = typeof text_y !== 'undefined' ? text_y : '-.10em';

    let dataset = {
        lower: [0, 100],
        upper: [percent, (100 - percent)],
      },

      radius = Math.min(width, height) / 2,
      pie = d3.layout.pie().sort(null),

      format = d3.format('.0%');

    const arc = d3.svg.arc()
      .innerRadius(radius - 20)
      .outerRadius(radius);

    const svg = d3.select(element).append('svg')
      .attr('width', width)
      .attr('height', height)
      .attr('class', 'piechart')
      .append('g')
      .attr('transform', `translate(${width / 2},${height / 2})`);

    let path = svg.selectAll('path')
      .data(pie(dataset.lower))
      .enter().append('path')
      .attr('class', function (d, i) {
        return `color${i}`;
      })
      .attr('d', arc)
      .each(function (d) {
        this._current = d;
      });

    const text = svg.append('text')
      .attr('text-anchor', 'middle')
      .attr('dy', text_y);

    if (typeof (percent) === 'string') {
      text.text(percent);
    } else {
      const progress = 0;
      var timeout = setTimeout(function () {
        clearTimeout(timeout);
        path = path.data(pie(dataset.upper));
        path.transition().duration(500).attrTween('d', function (a) {
          const i = d3.interpolate(this._current, a);
          const i2 = d3.interpolate(progress, percent);
          this._current = i(0);
          return function (t) {
            text.text(format(i2(t) / 100));
            return arc(i(t));
          };
        });
      }, 200);
    }
  }

  render() {
    return (
      <div id="rating-block-component" className="rating-block">
        <div id={this.props.type === 'positive' ? 'positive' : 'negative'} className="donut" />
        <div className={this.props.type === 'positive' ? 'positive label' : 'negative label'}>
          <i className={this.props.type === 'positive' ? 'gr-icon icon-thumbs-up-alt' : 'gr-icon icon-thumbs-down-alt'} />
          <span className="count" />
          {this.props.type === 'positive' ? 'Positive' : 'Negative'} Plays
      </div>
      </div>
    );
  }
};

COMPONENT.propTypes = {
};

export default COMPONENT;
