import React from 'react';
import PropTypes from 'prop-types';

function ListItem(props) {
  return (
    <div className="card style-accent-dark card-collapsed card-underline">
      <div className="card-head">
        <header><h1>{props.date}</h1></header>
        <div className="tools play-actions">
          <span className="start-time">{props.start}</span>
          <span className="end-time">{props.end}</span>
          <span>&nbsp;&nbsp;&nbsp;</span>
          <div onClick={() => { props.delete(props.practiceIndex); }} className="btn btn-icon-toggle btn-delete" rel="tooltip"><i className="gr-icon icon-trash-empty" /></div>
          <div onClick={() => { props.open(props.practiceIndex); }} className="btn btn-icon-toggle btn-open" rel="tooltip"><i className="gr-icon icon-arrow-up-right" /></div>
        </div>
      </div>
    </div>
  );
}
ListItem.displayName = 'ListItem';

export const COMPONENT = class StatsList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="stats-list-component" className="row mainview" style={{ display: 'block' }}>
        <div className="col-lg-12 statslist">
          <h2 className="center offset">Practice Stats &amp; Review</h2>
          {Object.keys(this.props.practices).map((key) => {
            const practice = this.props.practices[key];
            if (practice.plays) {
              return <ListItem open={this.props.open} delete={this.props.delete} key={key} practiceIndex={key} practice={practice} date={(new Date(practice.start)).toLocaleDateString()} start={(new Date(practice.start)).toLocaleTimeString()} end={(new Date(practice.end)).toLocaleTimeString()} />;
            }
          })}
        </div>
      </div>
    );
  }
};

COMPONENT.propTypes = {
};

export default COMPONENT;
