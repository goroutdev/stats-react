import React from 'react';
import PropTypes from 'prop-types';
import NumberEasing from 'react-number-easing';

export default class MetricDisplay extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="metric-display-component" >
        <div className="data">
          <NumberEasing value={this.props.data} speed={2000} ease='quintInOut' />
        </div>
        <div className="label" >{this.props.label}</div>
      </div>
    );
  }
}

MetricDisplay.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  label: PropTypes.string.isRequired,
};
