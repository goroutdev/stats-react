import React from 'react';
import PropTypes from 'prop-types';


function CardImage(props) {
  let icon = '';
  if (props.rating == 1 || props.rating == 0) {
    const iconClass = props.rating == 1 ? 'gr-icon icon-thumbs-up-alt' : 'gr-icon icon-thumbs-down-alt';
    icon = <i className={iconClass} />;
  }

  const imgHref = `${props.href}/src/sync/org${props.organizationId}/files/images/${props.image}`;

  return (
    <div className="card-header color-white no-border">
      <div className="playimagecontainer">
        <img src={imgHref} />
      </div>
      {icon}
    </div>
  );
}
CardImage.displayName = 'CardImage';

function CardContent(props) {
  return (
    <div className="card-content">
      <div className="card-content-inner">
        <p className="color-gray playname">{props.name}</p>
        <p className="playtime">{(props.time / 1000).toFixed(1)} seconds</p>
      </div>
    </div>
  );
}
CardContent.displayName = 'CardContent';

function CardFooter(props) {
  return (
    <div className="card-footer">
      <div className="row">
        <div className="col-50">
          <div onClick={() => { props.sendPlay(); }} id="stats-send-play-button" className="button button-big send">Send Play</div>
        </div>
        <div className="col-50">
          <div onClick={() => { props.changeRating(); }} id="stats-change-rating-button" className="button button-big rate">Change Rating</div>
        </div>
      </div>
    </div>
  );
}
CardFooter.displayName = 'CardFooter';

export const COMPONENT = class PlayCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.href) {
      this.props.href = window.location.origin;
    }
    return (
      <div id="play-card-component" className="stats-playcard">
        <CardImage href={this.props.href} organizationId={this.props.organizationId} image={this.props.image} rating={this.props.rating} />
        <CardContent name={this.props.name} time={this.props.time} />
        <CardFooter sendPlay={this.props.sendPlay} changeRating={this.props.changeRating} />
      </div>
    );
  }
};

COMPONENT.propTypes = {
  href: PropTypes.string,
  name: PropTypes.string.isRequired,
  organizationId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  image: PropTypes.string.isRequired,
  rating: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  time: PropTypes.number.isRequired,
  sendPlay: PropTypes.func,
  changeRating: PropTypes.func,
};


export default COMPONENT;
