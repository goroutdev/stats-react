import React from 'react';
import PropTypes from 'prop-types';
import RatingDisplay from './../Rating-Display/component';
import MetricDisplay from './../Metric-Display/component';

export const COMPONENT = class StatsHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  getReptimeAndCount() {
    let count = 0;
    let time = 0;
    const plays = this.props.plays;
    const callType = this.props.callTypeFilter;

    Object.keys(plays).forEach((key) => {
      const play = plays[key];
      if (callType == play.callType) {
        count++;
        time += play.interval;
      }
    });
    let avgTime = ((time / 1000) / count).toFixed(1);
    if (isNaN(avgTime)) {
      avgTime = 0;
    }

    return {
      avgTime,
      count,
    };
  }

  render() {
    const { avgTime, count } = this.getReptimeAndCount();
    return (
      <div className="statsheader">
        <MetricDisplay data={count} label="Play Count" />
        <MetricDisplay data={avgTime} label="Avg. Rep Time(seconds)" />
        <RatingDisplay plays={this.props.plays} type="positive" callTypeFilter={this.props.callTypeFilter} />
        <RatingDisplay plays={this.props.plays} type="negative" callTypeFilter={this.props.callTypeFilter} />
      </div>
    );
  }
};

COMPONENT.propTypes = {
};

export default COMPONENT;
