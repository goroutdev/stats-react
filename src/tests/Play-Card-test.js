import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';
import PlayCard from '../components/Play-Card/component';

const data = {
  _task: 'track_stats',
  _timestamp: 1495052348938,
  callType: 1,
  end: 1495055858248,
  image: 'playfile0_5903c7df05b20-6_send.jpg?1494461283000',
  interval: 12915,
  isRated: true,
  name: 'Spread Right Flex 2 H Trap Lead',
  organizationId: 2,
  playId: 1691,
  practiceId: 54,
  rating: 1,
  start: 1495055845333,
};


describe('Card Image', function () {
  const wrapper = mount(<PlayCard href="https://air.gorout.com" organizationId={2} image="playfile0_5903c7df05b20-6_send.jpg?1494461283000" time={12915} rating={1} name="Spread Right Flex 2 H Trap Lead" />);
  const cardImage = wrapper.find('CardImage');

  it('displays the thumbs up icon on positve plays', function () {
    expect(cardImage.find('.icon-thumbs-up-alt')).to.have.length(1);
  });

  it('displays the thumbs down icon on negative plays', function () {
    wrapper.setProps({ rating: 0 });
    expect(cardImage.find('.icon-thumbs-down-alt')).to.have.length(1);
  });

  it('displays no icon when play is unrated', function () {
    wrapper.setProps({ rating: null });
    expect(cardImage.find('.gr-icon')).to.have.length(0);
  });

  it('has the correct image url', function () {
    const img = cardImage.find('img');
    expect(img.html()).to.equal('<img src="https://air.gorout.com/src/sync/org2/files/images/playfile0_5903c7df05b20-6_send.jpg?1494461283000">');
  });
});

describe('Card Content', function () {
  const wrapper = mount(<PlayCard href="https://air.gorout.com" organizationId={2} image="playfile0_5903c7df05b20-6_send.jpg?1494461283000" time={12915} rating={1} name="Spread Right Flex 2 H Trap Lead" />);
  const cardContent = wrapper.find('CardContent');

  it('displays the name of the play', function () {
    expect(cardContent.find('.playname').text()).to.equal('Spread Right Flex 2 H Trap Lead');
  });

  it('displays the time of the play', function () {
    expect(cardContent.find('.playtime').text()).to.equal('12.9 seconds');
  });
});
