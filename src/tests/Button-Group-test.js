import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';
import ButtonGroup from '../components/Button-Group/component';

const data = [
  { text: 'Row 1' }, { text: 'Row 2' }, { text: 'Row 3' }, { text: 'Row 4' }, { text: 'Row 5' },
];

describe('Button Container', function () {
  const wrapper = mount(<ButtonGroup data={data} />);
  const buttonContainer = wrapper.find('ButtonContainer');

  it('contains class btn-group by default', function () {
    expect(buttonContainer.hasClass('btn-group')).to.equal(true);
  });

  it('adds active class when clicked', function () {
    wrapper.find('.btn.type-title-arrow').simulate('click');
    expect(buttonContainer.hasClass('active')).to.equal(true);
  });
});

describe('Button Header', function () {
  sinon.spy(ButtonGroup.prototype, 'openList');
  const wrapper = mount(<ButtonGroup data={data} />);
  const buttonHeader = wrapper.find('ButtonHeader');

  it('displays the first record as the default text', function () {
    expect(buttonHeader.containsAnyMatchingElements([<button>Row 1</button>])).to.equal(true);
  });

  it('calls openList when clicked', function () {
    buttonHeader.find('.btn.type-title-arrow').simulate('click');
    expect(ButtonGroup.prototype.openList.calledOnce).to.equal(true);
  });

  it('changes the header when a list item is clicked', function () {
    const listItems = wrapper.find('.listItemClickTest').simulate('click');
    expect(buttonHeader.containsAnyMatchingElements([<button>Row 4</button>])).to.equal(true);
  });
});

describe('List', function () {
  // set spy

  const wrapper = mount(<ButtonGroup data={data} />);
  const List = wrapper.find('List');

  it('renders 5 ListItem Components', function () {
    expect(List.find('ListItem')).to.have.length(5);
  });
});

describe('List Item', function () {
  // set spy
  sinon.spy(ButtonGroup.prototype, 'setActive');
  const wrapper = mount(<ButtonGroup data={data} />);
  const ListItem = wrapper.find('ListItem').first();

  it('displays the text property', function () {
    expect(ListItem.text()).to.equal(' Row 1');
  });

  it('has the icon-circle class by default on first list item', function () {
    expect(ListItem.find('i').hasClass('icon-circle')).to.equal(true);
  });

  it('has all other list items have icon-circle-thin class', function () {
    expect(wrapper.find('ListItem .icon-circle-thin')).to.have.length(4);
  });

  it('calls setActive method on click', function () {
    wrapper.find('.listItemClickTest').simulate('click');
    expect(ButtonGroup.prototype.setActive.calledOnce).to.equal(true);
  });

  it('switches the selected ListItem to have icon-circle class', function () {

  });
});

describe('Button Group', function () {
  const wrapper = mount(<ButtonGroup data={data} />);

  it('renders the Button Container component', function () {
    expect(wrapper.find('ButtonContainer')).to.have.length(1);
  });

  it('renders the Button Header component', function () {
    expect(wrapper.find('ButtonHeader')).to.have.length(1);
  });

  it('renders the list component', function () {
    expect(wrapper.find('List')).to.have.length(1);
  });

  it('renders 5 List Item components', function () {
    expect(wrapper.find('ListItem')).to.have.length(5);
  });

  it('has a data property that is an array with 5 items', function () {
    expect(wrapper.props().data).to.have.length(5);
  });

  it('has a default state of active = false', function () {
    expect(wrapper.state().active).to.equal(false);
  });

  it('has a default state of activeIndex = 0', function () {
    expect(wrapper.state().activeIndex).to.equal(0);
  });

  it('changes active state when button header is clicked', function () {
    wrapper.find('.btn.type-title-arrow').simulate('click');
    expect(wrapper.state().active).to.equal(true);
  });

  it('changes activeIndex state when ListItem is clicked', function () {
    wrapper.find('.listItemClickTest').simulate('click');
    expect(wrapper.state().activeIndex).to.equal(3);
  });

  it('changes active state back to false after ListItem click', function () {
    expect(wrapper.state().active).to.equal(false);
  });
});
