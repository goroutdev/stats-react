import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';
import MetricDisplay from '../components/Metric-Display/component';

describe('Metric Display', function () {
  const wrapper = mount(<MetricDisplay data={10} label="Test Label" />);

  it('should display label property as the label', function () {
    expect(wrapper.find('.label').text()).to.equal('Test Label');
  });

  it('should display data property as the metric', function () {
    expect(wrapper.find('.data').text()).to.equal('10');
  });

  it('should update display when propety is changed', function () {
    wrapper.setProps({ label: 'Update Label' });
    expect(wrapper.find('.label').text()).to.equal('Update Label');
  });
});
