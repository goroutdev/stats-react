// IMPORTANT
// ---------
// This is an auto generated file with React CDK.
// Do not modify this file.

import { setAddon, configure } from '@kadira/storybook';

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
